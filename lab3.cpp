#include <iostream>
#include <cstring>
#include <cassert>
#include <sys/time.h>
#include <mpi.h>

#define ROWS_A 2000
#define COLUMNS_A 2000
#define ROWS_B 2000
#define COLUMNS_B 2000

#define X 0
#define Y 1

using namespace std;

void printMatrix (double *matrix, int columns, int rows) {
    for (int i = 0; i < columns; i++) {
        for (int j = 0; j < rows; j++) {
            cout << matrix[i * rows + j] << " ";
        }
        cout << endl;
    }
}

void printVector(int *vector, int size) {
    for (int i = 0; i < size; i++) {
        cout << vector[i] << " ";
    }
    cout << endl;
}

void init_matrix(double *matrixA, double *matrixB, double *matrixC) {
    memset(matrixA, 1, sizeof(double) * COLUMNS_A * ROWS_A);
    memset(matrixB, 2, sizeof(double) * COLUMNS_B * ROWS_B);
    memset(matrixC, 0, sizeof(double) * COLUMNS_B * ROWS_A);
}

void matrix_mul(double *matrixA, double *matrixB, double *matrixC, int rowsA, int colA, int colB) {
    for (int i = 0; i < rowsA; ++i) {   // colA = rowsB
        double *c = matrixC + i * colB;
        for (int j = 0; j < colB; ++j)
            c[j] = 0;
        for (int k = 0; k < colA; ++k) {
            double *b = matrixB + k * colB;
            double a = matrixA[i * colA + k];
            for (int j = 0; j < colB; ++j)
                c[j] += a * b[j];
        }
    }
}

void memFree(double *matrixA, double *matrixB, double *matrixC,double *curMatrixA, double *curMatrixB, double *curMatrixC) {
    delete[] matrixA;
    delete[] matrixB;
    delete[] matrixC;
    delete[] curMatrixA;
    delete[] curMatrixB;
    delete[] curMatrixC;
}

int main(int argc, char** argv) {
    struct timeval tv1, tv2;
    gettimeofday(&tv1, NULL);
    assert(COLUMNS_A == ROWS_B); // согласованы

    MPI_Init(&argc, &argv);

    int sizeProc;
    int myRank;
    
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
    MPI_Comm_size(MPI_COMM_WORLD, &sizeProc);

    double *matrixA = nullptr;
    double *matrixB = nullptr;
    double *matrixC = nullptr;

    if (myRank == 0) {
        matrixA = new double[COLUMNS_A * ROWS_A];
        matrixB = new double[COLUMNS_B * ROWS_B];
        matrixC = new double[COLUMNS_B * ROWS_A];

        init_matrix(matrixA, matrixB, matrixC);
    }
    // создаем коммуникатор - двумерную решетку
    int dims[2] = {0, 0};       // размер решетки (кол-во процессов по каждой координате)
    int periods[2] = {0, 0};    // нет периода
    MPI_Comm grid;				// просто сетка
    MPI_Dims_create(sizeProc, 2, dims);  // размерность сетки
    MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, 1, &grid); //создал топологию двумерной решетки

	int cordsInGrid[2]; // координаты на сетке
    MPI_Cart_coords(grid, myRank, 2, cordsInGrid); //каждый процесс получил свои кординаты в двумерной решетке

    double *curMatrixA = new double[COLUMNS_A * ROWS_A / dims[Y]]();
    double *curMatrixB = new double[COLUMNS_B * ROWS_B / dims[X]]();
    double *curMatrixC = new double[COLUMNS_B * ROWS_A / (dims[X] * dims[Y])](); //выделяю локальные матрицы

    MPI_Comm comm_row;
    MPI_Comm comm_column;
    int rowSeparator[2] = {1, 0}; // по строкам
    int columnSeparator[2] = {0, 1}; // по столбцам
    MPI_Cart_sub(grid, rowSeparator, &comm_row);       //разбил всю топологию на отдельные строки
    MPI_Cart_sub(grid, columnSeparator, &comm_column); //разбил всю топологию на отдельные столбцы

    if (cordsInGrid[X] == 0) //разослал строки
        MPI_Scatter(matrixA, ROWS_A * COLUMNS_A / dims[Y], MPI_DOUBLE, curMatrixA, ROWS_A * COLUMNS_A / dims[Y],
                    MPI_DOUBLE, 0, comm_column);


    if (cordsInGrid[Y] == 0) { //разослал столбцы
        MPI_Datatype vectorT;
        MPI_Datatype resized_vectorT;

        MPI_Type_vector(ROWS_B, COLUMNS_B / dims[X], COLUMNS_B, MPI_DOUBLE, &vectorT);
        MPI_Type_create_resized(vectorT, 0, COLUMNS_B / dims[X] * sizeof(double), &resized_vectorT);
        MPI_Type_commit(&resized_vectorT); // объявлен

        MPI_Scatter(matrixB, 1, resized_vectorT, curMatrixB, ROWS_B * COLUMNS_B / dims[X], MPI_DOUBLE, 0, comm_row);

        MPI_Type_free(&resized_vectorT);
    }

    MPI_Bcast(curMatrixA, ROWS_A * COLUMNS_A / dims[Y], MPI_DOUBLE, 0, comm_row);
    MPI_Bcast(curMatrixB, ROWS_B * COLUMNS_B / dims[X], MPI_DOUBLE, 0, comm_column); //раскидал кусочки матрицы по узлам

    matrix_mul(curMatrixA, curMatrixB, curMatrixC, COLUMNS_A / dims[Y], ROWS_A,
               COLUMNS_B / dims[X]); 

    int recvCounts[sizeProc];
    int displace[sizeProc];
    for (int i = 0; i < sizeProc; i++) {
        recvCounts[i] = COLUMNS_B * ROWS_A / (dims[X] * dims[Y]);

        MPI_Cart_coords(grid, i, 2, cordsInGrid);
        displace[i] = dims[X] * (ROWS_A / dims[Y]) * cordsInGrid[Y] + cordsInGrid[X];
    }

    MPI_Datatype recv_vectorT;
    MPI_Datatype resized_recv_vectorT;

    MPI_Type_vector(ROWS_A / dims[Y], COLUMNS_B / dims[X], COLUMNS_B, MPI_DOUBLE, &recv_vectorT);
    MPI_Type_create_resized(recv_vectorT, 0, COLUMNS_B / dims[X] * sizeof(double), &resized_recv_vectorT);
    MPI_Type_commit(&resized_recv_vectorT); //сделал тип вектора для приема на 0 узле

    MPI_Gatherv(curMatrixC, COLUMNS_B * ROWS_A / (dims[X] * dims[Y]), MPI_DOUBLE, matrixC, recvCounts, displace,
                  resized_recv_vectorT, 0, grid); //отправил и собрал итоговую матрицу на 0 узле

    if (myRank == 0)
        printMatrix(matrixC, COLUMNS_B, ROWS_A); 

    memFree(matrixA, matrixB, matrixC, curMatrixA, curMatrixB, curMatrixC);
    MPI_Comm_free(&grid);
    MPI_Comm_free(&comm_row);
    MPI_Comm_free(&comm_column);
    MPI_Finalize();

    gettimeofday(&tv2, NULL);
    double sec = tv2.tv_sec - tv1.tv_sec;
    double usec = tv2.tv_usec - tv1.tv_usec;

    std::cout << "time: " << sec + (1e-6 * usec) << std::endl;

    return 0;
}


